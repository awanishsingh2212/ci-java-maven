import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Demo_Get_Request {
    public static void getWeatherDetails()
    {
        RestAssured.baseURI="http://restapi.demoqa.com/utilities/weather/city";
        RequestSpecification requestSpecification=RestAssured.given();
        Response response=requestSpecification.request(Method.GET,"/Patna");
        System.out.println(response.toString());
    }
    public static void main(String args[])
    {
        getWeatherDetails();
    }
}
