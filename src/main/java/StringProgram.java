import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringProgram {
    public static List panagram(String a){
        boolean a1[]=new boolean[26];
        int index=0;
        List<Character> ls=new ArrayList<>();

        for(int i=0;i<a.length();i++)
        {
            if(a.charAt(i)>='a' && a.charAt(i)<='z')
               index=a.charAt(i)-'a';
               System.out.println(index);
               a1[index]=true;
            if(a.charAt(i)>='A' && a.charAt(i)<='Z')
                 index=a.charAt(i)-'A';
                 System.out.println(index);
                 a1[index]=true;
        }
        for (int i=0;i<=25;i++)
        {
            if (!a1[i])
            {
                 ls.add((char)(i+'a'));
            }
        }
       return ls;
    }
    public static boolean duckNumber(String val)
    {
        int count=0;
        for (int i=0;i<val.length();i++)
        {
            if (val.charAt(i)=='0')
            {
                count+=1;
            }
            if (count>1)
            {
                return false;
            }



        }
        return true;

    }
    public static String longestSubSequence(String abc)
    {
        Map<Character,Integer> map=new HashMap<>();
        String output="";
        for(char c:abc.toCharArray())
        {
            if (map.containsKey(c))
            {
                map.put(c,map.get(c)+1);
            }
            else{
                map.put(c,1);
            }
        }
        return "";

    }
    public static void main(String args[])
    {
        System.out.println(panagram("The quick brown fox jumps over the lay dog"));
        System.out.println(duckNumber("02364"));
    }
}
